package org.leder.gldisplaymanager.model;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;

import java.time.LocalDate;

/**
 * Encapsulates an online offer from a webshop for a product
 */
public class Offer {
    //region Constants
    //endregion

    //region Attributs
    private int id;
    private DoubleProperty price;
    private ObjectProperty<LocalDate> date;
    private ObjectProperty<Display> display;
    private ObjectProperty<Webshop> webshop;
    //endregion

    //region Constructors
    public Offer() {

        this.id = -1;
        this.price.set(-1);
        this.date = null;
        this.display = null;
        this.webshop = null;

    }

    public Offer(double price, Display display, LocalDate date, Webshop webshop) {
//        this.id = id;
        this.price = new SimpleDoubleProperty(price);
        this.display = new SimpleObjectProperty<>(display);
        this.date = new SimpleObjectProperty<>(date);
        this.webshop = new SimpleObjectProperty<>(webshop);
    }


    public Offer(int id, double price, Display display, LocalDate date, Webshop webshop) {
        this(price, display, date, webshop);
        this.id = id;

    }

    //endregion

    //region Methods

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPrice() {
        return price.get();
    }

    public void setPrice(double price) {
        this.price.set(price);
    }

    public LocalDate getDate() {
        return date.get();
    }

    public void setDate(LocalDate date) {
        this.date.set(date);
    }

    public Display getDisplay() {
        return display.get();
    }

    public void setDisplay(Display display) {
        this.display.set(display);
    }

    public Webshop getWebshop() {
        return webshop.get();
    }

    public void setWebshop(Webshop webshop) {
        this.webshop.set(webshop);
    }

    public DoubleProperty priceProperty() {
        return price;
    }

    public ObjectProperty<LocalDate> dateProperty() {
        return date;
    }

    public ObjectProperty<Display> displayProperty() {
        return display;
    }

    public ObjectProperty<Webshop> webshopProperty() {
        return webshop;
    }

    @Override
    public String toString() {
        return "Offer{" +
                "id=" + id +
                ", price=" + price.get() +
                ", date=" + date.get() +
                ", display=" + display.get() +
                ", webshop=" + webshop.get() +
                '}';
    }
//endregion


}
