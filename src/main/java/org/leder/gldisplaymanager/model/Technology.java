package org.leder.gldisplaymanager.model;

/**
 * Encapsulates the different computer monitor display technologies
 */
public enum Technology {

    //region Enumeration
    OLED,
    QLED,
    IPS,
    TN,
    VA;
    //endregion

    //region Constants
    //endregion

    //region Attributs
    //endregion

    //region Constructors
    //endregion

    //region Methods

    //endregion

}
