package org.leder.gldisplaymanager.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Encapsulates the webshop presence on the WWW
 */
public class Webshop {
    //region Constants
    //endregion

    //region Attributs

    private int id;
    private StringProperty shopname;
    private StringProperty url;

    //endregion

    //region Constructors
    public Webshop() {
    }

    public Webshop(String shopname, String url) {
        this.shopname = new SimpleStringProperty(shopname);
        this.url = new SimpleStringProperty(url);
    }

    public Webshop(int id, String shopname, String url) {

        this(shopname, url);
        this.id = id;

    }
    //endregion

    //region Methods

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getShopname() {
        return shopname.get();
    }

    public void setShopname(String shopname) {
        this.shopname.set(shopname);
    }

    public String getUrl() {
        return url.get();
    }

    public void setUrl(String url) {
        this.url.set(url);
    }

    public StringProperty shopnameProperty() {
        return shopname;
    }

    public StringProperty urlProperty() {
        return url;
    }

    @Override
    public String toString() {
        return "Webshop{" +
                "id=" + id +
                ", shopname=" + shopname.get() +
                ", url=" + url.get() +
                '}';
    }

    //endregion


}
