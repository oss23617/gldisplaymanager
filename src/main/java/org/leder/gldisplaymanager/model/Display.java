package org.leder.gldisplaymanager.model;

import javafx.beans.property.*;

/**
 * Encapsulates Attributes and Methods for the product computer display
 */

public class Display {
    //region Constants
    //endregion

    //region Attributs
    private int id;
    private StringProperty manufacturer;
    private StringProperty modelName;
    private IntegerProperty modelYear;
    private ObjectProperty<Technology> technology;
    private DoubleProperty screenSize;
    private IntegerProperty refreshRate;

    //endregion

    //region Constructors
    public Display() {
        this.id = -1;
        this.refreshRate = new SimpleIntegerProperty(-1);
        this.screenSize = new SimpleDoubleProperty(-1);
        this.technology = new SimpleObjectProperty<>(null);
        this.modelYear = new SimpleIntegerProperty(-1);
        this.modelName = new SimpleStringProperty(">not set<");
        this.manufacturer = new SimpleStringProperty(">not set<");
    }
    public Display(int refreshRate, double screenSize, Technology technology, int modelYear, String modelName, String manufacturer) {
        this.refreshRate = new SimpleIntegerProperty(refreshRate);
        this.screenSize = new SimpleDoubleProperty(screenSize);
        this.technology = new SimpleObjectProperty<>(technology);
        this.modelYear = new SimpleIntegerProperty(modelYear);
        this.modelName = new SimpleStringProperty(modelName);
        this.manufacturer = new SimpleStringProperty(manufacturer);
    }


    public Display(int id, int refreshRate, double screenSize, Technology technology, int modelYear, String modelName, String manufacturer) {
        this(refreshRate, screenSize, technology, modelYear, modelName, manufacturer);
        this.id = id;

    }
    //endregion

    //region Methods

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getManufacturer() {
        return manufacturer.get();
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer.set(manufacturer);
    }

    public String getModelName() {
        return modelName.get();
    }

    public void setModelName(String modelName) {
        this.modelName.set(modelName);
    }

    public int getModelYear() {
        return modelYear.get();
    }

    public void setModelYear(int modelYear) {
        this.modelYear.set(modelYear);
    }

    public Technology getTechnology() {
        return technology.get();
    }

    public void setTechnology(Technology technology) {
        this.technology.set(technology);
    }

    public double getScreenSize() {
        return screenSize.get();
    }

    public void setScreenSize(double screenSize) {
        this.screenSize.set(screenSize);
    }

    public int getRefreshRate() {
        return refreshRate.get();
    }

    public void setRefreshRate(int refreshRate) {
        this.refreshRate.set(refreshRate);
    }

    public StringProperty manufacturerProperty() {
        return manufacturer;
    }

    public StringProperty modelNameProperty() {
        return modelName;
    }

    public IntegerProperty modelYearProperty() {
        return modelYear;
    }

    public ObjectProperty<Technology> technologyProperty() {
        return technology;
    }

    public DoubleProperty screenSizeProperty() {
        return screenSize;
    }

    public IntegerProperty refreshRateProperty() {
        return refreshRate;
    }

    @Override
    public String toString() {
        return "Display{" +
                "id=" + id +
                ", manufacturer=" + manufacturer.get() +
                ", modelName=" + modelName.get() +
                ", modelYear=" + modelYear.get() +
                ", technology=" + technology.get() +
                ", screenSize=" + screenSize.get() +
                ", refreshRate=" + refreshRate.get() +
                '}';
    }

//endregion


}
