package org.leder.gldisplaymanager.logic;

import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.util.Callback;
import org.leder.gldisplaymanager.logic.database.DbManager;
import org.leder.gldisplaymanager.model.Offer;
import org.leder.gldisplaymanager.test.TestData;

import java.util.Comparator;

public class OfferManager {
    //region Constants
    //endregion

    //region Attributs
    private static OfferManager instance;
    private ObservableList<Offer> offers;

    private boolean sortReversed = false;
    private int numericSortToggle = 1;

    //endregion

    //region Constructors
    private OfferManager() {

        offers = FXCollections.observableList(
                 DbManager.getInstance().readAllOffers(), new Callback<Offer, Observable[]>() {
                    @Override
                    public Observable[] call(Offer offer) {
                        return new Observable[]{
                                offer.priceProperty(),
                                offer.dateProperty(),

                                offer.getDisplay().manufacturerProperty(),
                                offer.getDisplay().modelNameProperty(),
                                offer.getDisplay().modelYearProperty(),
                                offer.getDisplay().refreshRateProperty(),
                                offer.getDisplay().screenSizeProperty(),
                                offer.getDisplay().technologyProperty(),

                                offer.getWebshop().urlProperty(),
                                offer.getWebshop().shopnameProperty()

                        };
                    }
                });
        //TestData.getTestOffers());

        offers.addListener((ListChangeListener<Offer>) change -> {
            if (change.next()) {
                if (change.wasReplaced()) {
                    //Logik, wenn das Aktualisieren von Tieren durch das Ersetzen implementiert
                } else if (change.wasAdded()) {
                    Offer offerToCreate = change.getAddedSubList().get(0);
                    DbManager.getInstance().insert(offerToCreate);
                } else if (change.wasRemoved()) {

                    Offer offerToRemove = change.getRemoved().get(0);
                    DbManager.getInstance().delete(offerToRemove);

                } else if (change.wasUpdated()) {

                    int index = change.getFrom();
                    Offer offerToUpdate = change.getList().get(index);//animals.get(index);
                    DbManager.getInstance().update(offerToUpdate);

                } else if (change.wasPermutated()) {
                    System.out.println("Sorted!");
                }
            }

        });
    }

    //endregion

    //region Methods
    public static synchronized OfferManager getInstance() {
        if (instance == null) instance = new OfferManager();
        return instance;
    }

    public ObservableList<Offer> getOffers() {
        return offers;
    }

    public ObservableList<Offer> filterBy(String value) {
        return offers.filtered(offer -> offer.getDisplay().getManufacturer().toLowerCase().contains(value.toLowerCase())
                || offer.getDisplay().getModelName().toLowerCase().contains(value.toLowerCase())
                || String.valueOf(offer.getDisplay().getModelYear()).toLowerCase().contains(value.toLowerCase())
                || String.valueOf(offer.getDisplay().getRefreshRate()).toLowerCase().contains(value.toLowerCase())
                || offer.getDisplay().getTechnology().toString().toLowerCase().contains(value.toLowerCase())
                || String.valueOf(offer.getDisplay().getScreenSize()).toLowerCase().contains(value.toLowerCase())
                || String.valueOf(offer.getDate()).toLowerCase().contains(value.toLowerCase())
                || String.valueOf(offer.getPrice()).toLowerCase().contains(value.toLowerCase()));

    }

    public void sortByManufacturer() {
        //Ausführliche Schreibweise
        offers.sort(new Comparator<Offer>() {
            @Override
            public int compare(Offer offerOne, Offer offerTwo) {
                return numericSortToggle * offerOne.getDisplay().getManufacturer().compareTo(offerTwo.getDisplay().getManufacturer());
            }
        });

        numericSortToggle *= -1;
    }

    public void sortByName() {
        //Schreibweise mit inner class
        offers.sort(new Comparator<Offer>() {
            @Override
            public int compare(Offer offerOne, Offer offerTwo) {
                return numericSortToggle * offerOne.getDisplay().getModelName().compareTo(offerTwo.getDisplay().getModelName());
            }
        });

        numericSortToggle *= -1;

//        if (sortReversed) offers.sort((offerOne, offerTwo) -> (offerOne.getDisplay().getModelName().compareTo(offerTwo.getDisplay().getModelName())));
//        else offers.sort((offerOne, offerTwo) -> (offerOne.getDisplay().getModelName().compareTo(offerTwo.getDisplay().getModelName())));

//        if (sortReversed) offers.sort(Comparator.comparing(Offer::getDisplay::getModelName).reversed());
//        else animals.sort(Comparator.comparing(Animal::getName));

//        sortReversed = !sortReversed;
    }

    public void sortByModelYear() {
        //Lambda-Schreibweise
        offers.sort((offerOne, offerTwo) -> (offerOne.getDisplay().getModelYear() - offerTwo.getDisplay().getModelYear()) * numericSortToggle);


        numericSortToggle *= -1;
    }

    public void sortByDate() {

        //inner class
        offers.sort(new Comparator<Offer>() {
            @Override
            public int compare(Offer offerOne, Offer offerTwo) {
                return numericSortToggle * offerOne.getDate().compareTo(offerTwo.getDate());
            }
        });

        numericSortToggle *= -1;
        //        if (sortReversed) offers.sort((offerOne, offerTwo) -> (offerOne.getDate().compareTo(offerTwo.getDate())));
//        else offers.sort((offerOne, offerTwo) -> (offerOne.getDate().compareTo(offerTwo.getDate())));
//
//        sortReversed = !sortReversed;
    }

    public void sortByManufacturerModelYear() {
        offers.sort((offerOne, offerTwo) -> {

            int manufacturerCompareResult = offerOne.getDisplay().getManufacturer().compareTo(offerTwo.getDisplay().getManufacturer());

            if (manufacturerCompareResult != 0) return manufacturerCompareResult;

            int modelCompareResult = offerOne.getDisplay().getModelName().compareTo(offerTwo.getDisplay().getModelName());

            if (modelCompareResult != 0) return modelCompareResult;

            return (offerOne.getDisplay().getModelYear() - offerTwo.getDisplay().getModelYear());

        });
//
//        offers.sort(Comparator.comparing(Offer::getDisplay)
//                .thenComparing(Animal::getName)
//                .thenComparingInt(Animal::getAge).reversed());
    }

    //endregion


}
