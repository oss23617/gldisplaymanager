package org.leder.gldisplaymanager.logic;

import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.util.Callback;
import org.leder.gldisplaymanager.logic.database.DbManager;
import org.leder.gldisplaymanager.model.Display;
import org.leder.gldisplaymanager.model.Webshop;

import java.util.Comparator;

public class WebshopManager {
    //region Constants
    //endregion

    //region Attributs
    private static WebshopManager instance;
    private ObservableList<Webshop> webshops;

    private boolean sortReversed = false;
    private int numericSortToggle = 1;

    //endregion

    //region Constructors
    private WebshopManager() {

        webshops = FXCollections.observableList(
                 DbManager.getInstance().readAllWebshops(), new Callback<Webshop, Observable[]>() {
                    @Override
                    public Observable[] call(Webshop webshop) {
                        return new Observable[]{

                                webshop.shopnameProperty(),
                                webshop.urlProperty()
                        };
                    }
                });
        //TestData.getTestOffers());

        webshops.addListener((ListChangeListener<Webshop>) change -> {
            if (change.next()) {
                if (change.wasReplaced()) {
                    //Logik, wenn das Aktualisieren von Tieren durch das Ersetzen implementiert
                } else if (change.wasAdded()) {
                    Webshop webshopToCreate = change.getAddedSubList().get(0);
                    DbManager.getInstance().insert(webshopToCreate);
                } else if (change.wasRemoved()) {

                    Webshop webshopToRemove = change.getRemoved().get(0);
                    DbManager.getInstance().delete(webshopToRemove);

                } else if (change.wasUpdated()) {

                    int index = change.getFrom();
                    Webshop webshopToUpdate = change.getList().get(index);//animals.get(index);
                    DbManager.getInstance().update(webshopToUpdate);

                } else if (change.wasPermutated()) {
                    System.out.println("Sorted!");
                }
            }

        });
    }

    //endregion

    //region Methods
    public static synchronized WebshopManager getInstance() {
        if (instance == null) instance = new WebshopManager();
        return instance;
    }

    public ObservableList<Webshop> getWebshops() {
        return webshops;
    }

    public ObservableList<Webshop> filterBy(String value) {
        return webshops.filtered(webshop -> webshop.getShopname().toLowerCase().contains(value.toLowerCase())
                || webshop.getUrl().toLowerCase().contains(value.toLowerCase())
        );
    }


}

    //endregion


