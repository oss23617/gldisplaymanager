package org.leder.gldisplaymanager.logic.database;

import java.sql.Connection;
import java.util.List;

/**
 * DAO - Data Access Object - Interface für Klassen,
 * welche Zugriff auf bestimmte Daten haben sollen.
 * Stellt hier Methoden für die grundlegenden CRUD-Operationen
 * (CREATE, READ, UPDATE, DELETE) zur Verfügung.
 */
public interface Dao<T> {
    void create(Connection connection, T object);

    List<T> readAll(Connection connection);

    void update(Connection connection, T object);

    void delete(Connection connection, T object);
}