package org.leder.gldisplaymanager.logic.database;

import org.leder.gldisplaymanager.gui.AlertManager;
import org.leder.gldisplaymanager.model.Display;
import org.leder.gldisplaymanager.model.Offer;

import org.leder.gldisplaymanager.model.Technology;
import org.leder.gldisplaymanager.model.Webshop;
import org.leder.gldisplaymanager.settings.AppTexts;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DaoOffer implements Dao<Offer>{

    public static final String SELECT_ALL_FROM_OFFER = "SELECT * FROM ((`offer` INNER JOIN display ON offer.display_id=display.id) INNER JOIN webshop ON offer.webshop_id=webshop.id)";
    public static final String COL_ID = "id";
    public static final String COL_PRICE = "price";
    public static final String COL_DATE = "date";

    public static final String COL_WEBSHOP_URL = "URL";
    public static final String COL_WEBSHOP_ID = "webshop_id";
    public static final String COL_WEBSHOP_NAME = "shopname";

    public static final String COL_DISPLAY_ID = "display_id";
    public static final String COL_DISPLAY_MANUFACTURER = "manufacturer";
    public static final String COL_DISPLAY_MODELNAME = "modelname";
    public static final String COL_DISPLAY_MODELYEAR = "modelyear";
    public static final String COL_DISPLAY_TECHNOLOGY = "technology";
    public static final String COL_DISPLAY_SCREENSIZE = "screensize";
    public static final String COL_DISPLAY_REFRESHRATE = "refreshrate";



    public static final String INSERT_INTO_OFFER = "INSERT INTO offer (price, date, display_id, webshop_id) VALUES (?,?,?,?)";
    public static final String INSERT_INTO_DISPLAY = "INSERT INTO display (refreshrate, screensize, technology, modelyear, modelname, manufacturer) VALUES (?,?,?,?,?,?)";
    public static final String INSERT_INTO_WEBSHOP = "INSERT INTO webshop (URL, shopname) VALUES (?,?);";

    public static final String DELETE_FROM_OFFER = "DELETE FROM offer WHERE id=?";
    public static final String UPDATE_OFFER_SET = "UPDATE offer SET price = ?, date = ?, display_id = ?, webshop_id = ? WHERE id = ?";
    public static final String UPDATE_DISPLAY_SET = "UPDATE display SET refreshrate = ?, screensize = ?, technology = ?, modelyear = ?, modelname = ?, manufacturer = ? WHERE id = ?";
    public static final String UPDATE_WEBSHOP_SET = "UPDATE webshop SET URL = ?, shopname = ? WHERE id = ?";


    //endregion

    //region Attributs
    //endregion

    //region Constructors
    public DaoOffer() {
    }

    @Override
    public void create(Connection connection, Offer offer){

        int display_id = -1;
        int webshop_id = -1;


//        try (PreparedStatement statement =
//                     connection.prepareStatement(
//                             INSERT_INTO_DISPLAY,
//                             Statement.RETURN_GENERATED_KEYS
//                     )
//        ) {
//
//            //            Platzhalter des Statements durch die richtigen Werte ersetzen
//
//            statement.setInt(1, offer.getDisplay().getRefreshRate());
//            statement.setDouble(2, offer.getDisplay().getScreenSize());
//            statement.setString(3, offer.getDisplay().getTechnology().name());
//            statement.setInt(4, offer.getDisplay().getModelYear());
//            statement.setString(5, offer.getDisplay().getModelName());
//            statement.setString(6, offer.getDisplay().getManufacturer());
//
//
//            //            Statement ausführen
//            statement.executeUpdate();
//
//            //            Auslesen der generierten Schlüssel (hier die ID des eingefügten Objektes)
//            ResultSet generatedKeys = statement.getGeneratedKeys();
//
//            //            Dem Objekt die in der Datenbank erzeugten ID zuweisen
//            if (generatedKeys.next()) {
//                display_id = generatedKeys.getInt("insert_id");
//                offer.getDisplay().setId(display_id);
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            AlertManager.getInstance().showErrorAlert(AppTexts.INSERT_IN_DATABASE_FAILED);
//
//        }
//
//        try (PreparedStatement statement =
//                     connection.prepareStatement(
//                             INSERT_INTO_WEBSHOP,
//                             Statement.RETURN_GENERATED_KEYS
//                     )
//        ) {
//
//            //            Platzhalter des Statements durch die richtigen Werte ersetzen
//
//            statement.setString(1, offer.getWebshop().getUrl());
//            statement.setString(2, offer.getWebshop().getShopname());
//
//            //            Statement ausführen
//            statement.executeUpdate();
//
//            //            Auslesen der generierten Schlüssel (hier die ID des eingefügten Objektes)
//            ResultSet generatedKeys = statement.getGeneratedKeys();
//
//            //            Dem Objekt die in der Datenbank erzeugten ID zuweisen
//
//            if (generatedKeys.next()) {
//
//                webshop_id = generatedKeys.getInt("insert_id");
//                offer.getWebshop().setId(webshop_id);
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            AlertManager.getInstance().showErrorAlert(AppTexts.INSERT_IN_DATABASE_FAILED);
//
//        }

        try (PreparedStatement statement =
                     connection.prepareStatement(
                             INSERT_INTO_OFFER,
                             Statement.RETURN_GENERATED_KEYS
                     )
        ) {
            display_id = offer.getDisplay().getId();
            webshop_id = offer.getWebshop().getId();

            //            Platzhalter des Statements durch die richtigen Werte ersetzen
            statement.setDouble(1, offer.getPrice());
            statement.setDate(2, Date.valueOf(offer.getDate()));

            statement.setInt(3, display_id);
            statement.setInt(4, webshop_id);

            //            Statement ausführen
            statement.executeUpdate();

            //            Auslesen der generierten Schlüssel (hier die ID des eingefügten Objektes)
            ResultSet generatedKeys = statement.getGeneratedKeys();

            //            Dem Objekt die in der Datenbank erzeugten ID zuweisen
            if (generatedKeys.next()) {
                offer.setId(generatedKeys.getInt("insert_id"));
            }

        } catch (Exception e) {
            e.printStackTrace();
            AlertManager.getInstance().showErrorAlert(AppTexts.INSERT_IN_DATABASE_FAILED);

        }


    }

//    @Override
//    public void create(Connection connection, Offer offer) {
//        try (PreparedStatement statement =
//                     connection.prepareStatement(
//                             INSERT_INTO_ANIMALS,
//                             Statement.RETURN_GENERATED_KEYS
//                     )
//        ) {
//
// //            Platzhalter des Statements durch die richtigen Werte ersetzen
//            statement.setString(1, offer.getSpecies());
//            statement.setString(2, offer.getName());
//            statement.setInt(3, offer.getAge());
//            statement.setString(4, offer.getColor());
//
// //            Statement ausführen
//            statement.executeUpdate();
//
// //            Auslesen der generierten Schlüssel (hier die ID des eingefügten Objektes)
//            ResultSet generatedKeys = statement.getGeneratedKeys();
//
// //            Dem Objekt die in der Datenbank erzeugten ID zuweisen
//            if (generatedKeys.next()) {
//                offer.setId(generatedKeys.getInt("insert_id"));
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            AlertManager.getInstance().showErrorAlert(AppTexts.INSERT_IN_DATABASE_FAILED);
//
//        }
//    }

    @Override
    public List<Offer> readAll(Connection connection) {
        List<Offer> offers = new ArrayList<>();

        //Statement Objekt über die Verbindung generieren und vorkompilieren lassen
        try (PreparedStatement statement = connection.prepareStatement(SELECT_ALL_FROM_OFFER)) {
            //Statement ausführen
            statement.execute();

            //Ergebnismenge des Statements auslesen
            ResultSet resultSet = statement.getResultSet();

            //Ergebnismenge iterieren und für jeden Datensatz ein Tierobjekt erzeugen
            while (resultSet.next()) {

                Offer offer = new Offer(
                        resultSet.getInt(COL_ID),
                        resultSet.getDouble(COL_PRICE),

                        new Display(
                                resultSet.getInt(COL_DISPLAY_ID),
                                resultSet.getInt(COL_DISPLAY_REFRESHRATE),
                                resultSet.getDouble(COL_DISPLAY_SCREENSIZE),
                                Technology.valueOf(resultSet.getString(COL_DISPLAY_TECHNOLOGY)),
                                resultSet.getInt(COL_DISPLAY_MODELYEAR),
                                resultSet.getString(COL_DISPLAY_MODELNAME),
                                resultSet.getString(COL_DISPLAY_MANUFACTURER)
                                ),

                        resultSet.getDate(COL_DATE).toLocalDate(),
                        new Webshop(
                                resultSet.getInt(COL_WEBSHOP_ID),
                                resultSet.getString(COL_WEBSHOP_NAME),
                                resultSet.getString(COL_WEBSHOP_URL)
                                )


                );

                offers.add(offer);
            }


        } catch (Exception e) {
            e.printStackTrace();
            AlertManager.getInstance().showErrorAlert(AppTexts.READ_FROM_DATABASE_FAILED);

        }

        return offers;
    }

    @Override
    public void update(Connection connection, Offer offer) {

        try (PreparedStatement statement = connection.prepareStatement(UPDATE_OFFER_SET)) {

            statement.setInt(5, offer.getId());
            statement.setDouble(1, offer.getPrice());
            statement.setDate(2, Date.valueOf(offer.getDate()));

            statement.setInt(3, offer.getDisplay().getId());
            statement.setInt(4, offer.getWebshop().getId());


            statement.executeUpdate();

        } catch (Exception ex) {
            ex.printStackTrace();
            AlertManager.getInstance().showErrorAlert(AppTexts.UPDATE_IN_DATABASE_FAILED);

        }

//        try (PreparedStatement statement = connection.prepareStatement(UPDATE_DISPLAY_SET)) {
//
//            statement.setInt(7, offer.getDisplay().getId());
//
//            statement.setInt(1, offer.getDisplay().getRefreshRate());
//            statement.setDouble(2, offer.getDisplay().getScreenSize());
//            statement.setString(3, offer.getDisplay().getTechnology().name());
//            statement.setInt(4, offer.getDisplay().getModelYear());
//            statement.setString(5, offer.getDisplay().getModelName());
//            statement.setString(6, offer.getDisplay().getManufacturer());
//
//
//            statement.executeUpdate();
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            AlertManager.getInstance().showErrorAlert(AppTexts.UPDATE_IN_DATABASE_FAILED);
//
//        }
//
//        try (PreparedStatement statement = connection.prepareStatement(UPDATE_WEBSHOP_SET)) {
//
//            statement.setInt(3, offer.getWebshop().getId());
//            statement.setString(1, offer.getWebshop().getUrl());
//            statement.setString(2, offer.getWebshop().getShopname());
//
//            statement.executeUpdate();
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            AlertManager.getInstance().showErrorAlert(AppTexts.UPDATE_IN_DATABASE_FAILED);
//
//        }

    }

    @Override
    public void delete(Connection connection, Offer offer) {

        try (PreparedStatement statement = connection.prepareStatement(DELETE_FROM_OFFER)){

            statement.setInt(1, offer.getId());

            statement.executeUpdate();

        } catch (Exception ex) {
            ex.printStackTrace();
            AlertManager.getInstance().showErrorAlert(AppTexts.DELETE_FROM_DATABASE_FAILED);

        }
    }

//    @Override
//    public void update(Connection connection, Animal animal) {
//
//        try (PreparedStatement statement = connection.prepareStatement(UPDATE_ANIMALS_SET)) {
//
//            statement.setInt(5, animal.getId());
//            statement.setString(1, animal.getSpecies());
//            statement.setString(2, animal.getName());
//            statement.setInt(3, animal.getAge());
//            statement.setString(4, animal.getColor());
//
//            statement.executeUpdate();
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            AlertManager.getInstance().showErrorAlert(AppTexts.UPDATE_IN_DATABASE_FAILED);
//
//        }
//
//    }
//
//    @Override
//    public void delete(Connection connection, Animal animal) {
//
//        try (PreparedStatement statement = connection.prepareStatement(DELETE_FROM_ANIMALS)){
//
//            statement.setInt(1, animal.getId());
//
//            statement.executeUpdate();
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            AlertManager.getInstance().showErrorAlert(AppTexts.DELETE_FROM_DATABASE_FAILED);
//
//        }
//    }
//    //endregion
//
    //region Methods
    //endregion


}