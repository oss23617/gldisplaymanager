package org.leder.gldisplaymanager.logic.database;

import org.leder.gldisplaymanager.gui.AlertManager;
import org.leder.gldisplaymanager.model.Display;
import org.leder.gldisplaymanager.model.Offer;
import org.leder.gldisplaymanager.model.Webshop;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;

public class DbManager {
    //region Constants

    private static final String HOST_NAME = "localhost/";
    private static final String DB_NAME = "display_manager";
    private static final String DB_USER = "root";
    private static final String DB_PW = "qwertz";

    public static final String JDBC_MARIADB = "jdbc:mariadb://";
    private static final String URL = JDBC_MARIADB + HOST_NAME + DB_NAME;

    public static final String DATABASE_CONNECTION_FAILED = "DB Connection failed!";
    //endregion

    //region Attributes
    private static DbManager instance;
    private DaoOffer offerDao;
    private DaoDisplay displayDao;
    private DaoWebshop webshopDao;
    //endregion

    //region Constructors
    private DbManager() {

        offerDao = new DaoOffer();
        displayDao = new DaoDisplay();
        webshopDao = new DaoWebshop();

//try{
      //  	Class.forName("org.mariadb.jdbc.Driver");
//}catch (Exception ex) {
	//		ex.printStackTrace();
	//	}

    }
    //endregion

    //region Methods
    public static synchronized DbManager getInstance() {
        if (instance == null) instance = new DbManager();
        return instance;
    }

    private Connection getConnection() {
        Connection connection = null;

        try {
            connection = DriverManager.getConnection(URL, DB_USER, DB_PW);
        } catch (Exception e) {
            e.printStackTrace();
            AlertManager.getInstance().showErrorAlert(DATABASE_CONNECTION_FAILED);

        }

        return connection;
    }


    public List<Offer> readAllOffers() {
        return offerDao.readAll(getConnection());
    }

    public void insert(Offer offer){
        offerDao.create(getConnection(), offer);
    }

    public void delete(Offer offerToRemove) {

        offerDao.delete(getConnection(), offerToRemove);
    }

    public void update(Offer offerToUpdate) {

        offerDao.update(getConnection(), offerToUpdate);

    }

    public List<Display> readAllDisplays() {
        return displayDao.readAll(getConnection());
    }

    public void insert(Display display){
        displayDao.create(getConnection(), display);
    }

    public void delete(Display displayToRemove) {

        displayDao.delete(getConnection(), displayToRemove);
    }

    public void update(Display displayToUpdate) {

        displayDao.update(getConnection(), displayToUpdate);

    }

    public List<Webshop> readAllWebshops() {
        return webshopDao.readAll(getConnection());
    }

    public void insert(Webshop webshop){
        webshopDao.create(getConnection(), webshop);
    }

    public void delete(Webshop webshopToRemove) {

        webshopDao.delete(getConnection(), webshopToRemove);
    }

    public void update(Webshop webshopToUpdate) {

        webshopDao.update(getConnection(), webshopToUpdate);

    }
    //endregion

}
