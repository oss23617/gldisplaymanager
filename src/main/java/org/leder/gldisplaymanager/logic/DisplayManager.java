package org.leder.gldisplaymanager.logic;

import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.util.Callback;
import org.leder.gldisplaymanager.logic.database.DbManager;
import org.leder.gldisplaymanager.model.Display;
import org.leder.gldisplaymanager.model.Offer;

import java.util.Comparator;

public class DisplayManager {
    //region Constants
    //endregion

    //region Attributs
    private static DisplayManager instance;
    private ObservableList<Display> displays;

    private boolean sortReversed = false;
    private int numericSortToggle = 1;

    //endregion

    //region Constructors
    private DisplayManager() {

        displays = FXCollections.observableList(
                 DbManager.getInstance().readAllDisplays(), new Callback<Display, Observable[]>() {
                    @Override
                    public Observable[] call(Display display) {
                        return new Observable[]{

                                display.manufacturerProperty(),
                                display.modelNameProperty(),
                                display.modelYearProperty(),
                                display.refreshRateProperty(),
                                display.screenSizeProperty(),
                                display.technologyProperty()

                        };
                    }
                });
        //TestData.getTestOffers());

        displays.addListener((ListChangeListener<Display>) change -> {
            if (change.next()) {
                if (change.wasReplaced()) {
                    //Logik, wenn das Aktualisieren von Tieren durch das Ersetzen implementiert
                } else if (change.wasAdded()) {
                    Display displayToCreate = change.getAddedSubList().get(0);
                    DbManager.getInstance().insert(displayToCreate);
                } else if (change.wasRemoved()) {

                    Display displayToRemove = change.getRemoved().get(0);
                    DbManager.getInstance().delete(displayToRemove);

                } else if (change.wasUpdated()) {

                    int index = change.getFrom();
                    Display displayToUpdate = change.getList().get(index);//animals.get(index);
                    DbManager.getInstance().update(displayToUpdate);

                } else if (change.wasPermutated()) {
                    System.out.println("Sorted!");
                }
            }

        });
    }

    //endregion

    //region Methods
    public static synchronized DisplayManager getInstance() {
        if (instance == null) instance = new DisplayManager();
        return instance;
    }

    public ObservableList<Display> getDisplays() {
        return displays;
    }

    public ObservableList<Display> filterBy(String value) {
        return displays.filtered(display -> display.getManufacturer().toLowerCase().contains(value.toLowerCase())
                || display.getModelName().toLowerCase().contains(value.toLowerCase())
                || String.valueOf(display.getModelYear()).toLowerCase().contains(value.toLowerCase())
                || String.valueOf(display.getRefreshRate()).toLowerCase().contains(value.toLowerCase())
                || display.getTechnology().toString().toLowerCase().contains(value.toLowerCase())
                || String.valueOf(display.getScreenSize()).toLowerCase().contains(value.toLowerCase())
        );
    }

    public void sortByManufacturer() {
        //Ausführliche Schreibweise
        displays.sort(new Comparator<Display>() {
            @Override
            public int compare(Display displayOne, Display displayTwo) {
                return numericSortToggle * displayOne.getManufacturer().compareTo(displayTwo.getManufacturer());
            }
        });

        numericSortToggle *= -1;
    }

    public void sortByName() {
        //Schreibweise mit inner class
        displays.sort(new Comparator<Display>() {
            @Override
            public int compare(Display displayOne, Display displayTwo) {
                return numericSortToggle * displayOne.getModelName().compareTo(displayTwo.getModelName());
            }
        });

        numericSortToggle *= -1;

//        if (sortReversed) offers.sort((offerOne, offerTwo) -> (offerOne.getDisplay().getModelName().compareTo(offerTwo.getDisplay().getModelName())));
//        else offers.sort((offerOne, offerTwo) -> (offerOne.getDisplay().getModelName().compareTo(offerTwo.getDisplay().getModelName())));

//        if (sortReversed) offers.sort(Comparator.comparing(Offer::getDisplay::getModelName).reversed());
//        else animals.sort(Comparator.comparing(Animal::getName));

//        sortReversed = !sortReversed;
    }

    public void sortByModelYear() {
        //Lambda-Schreibweise
        displays.sort((displayOne, displayTwo)-> (displayOne.getModelYear() - displayTwo.getModelYear()) * numericSortToggle);


        numericSortToggle *= -1;
    }

        //        if (sortReversed) offers.sort((offerOne, offerTwo) -> (offerOne.getDate().compareTo(offerTwo.getDate())));
//        else offers.sort((offerOne, offerTwo) -> (offerOne.getDate().compareTo(offerTwo.getDate())));
//
//        sortReversed = !sortReversed;

//
//        offers.sort(Comparator.comparing(Offer::getDisplay)
//                .thenComparing(Animal::getName)
//                .thenComparingInt(Animal::getAge).reversed());

}

    //endregion


