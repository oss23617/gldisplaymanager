package org.leder.gldisplaymanager.settings;

public class AppTexts {
    //region Constants
    public static final String APP_NAME = "Display Price  Manager!";
    public static final String OFFER_LIST_ENTRY = "%-15s %-15s %5s %15s";
    public static final String DELETE = "Delete";
    public static final String DELETE_CONFIRMATION_MESSAGE = "Do you really want to delete the following offer?";
    public static final String MANUFACTURER = "Manufacturer";
    public static final String MODEL_NAME = "Model Name";
    public static final String MODEL_YEAR = "Model Year";
    public static final String DATE = "Date";
    public static final String MANUFACTURER_MODEL_YEAR = "Manufacturer, Model, Year";
    public static final String UNKNOWN_SORT_BUTTON = "Unknown Sorting Button!";
    public static final String INSERT_OFFER_FAILED = "Insert Offer failed!";
    public static final String READ_ALL_OFFERS_FAILED = "Read all Offers failed!";
    public static final String UPDATE_OFFERS_FAILED = "Update Offer failed!";
    public static final String DELETE_OFFERS_FAILED = "Delete Offer failed!";
    public static final String INSERT_IN_DATABASE_FAILED = "Insert in DB failed!";
    public static final String READ_FROM_DATABASE_FAILED = "Read from DB failed!";
    public static final String UPDATE_IN_DATABASE_FAILED = "Update in DB failed!";
    public static final String DELETE_FROM_DATABASE_FAILED = "Delete from DB failed!";

    //endregion

    //region Attributs
    //endregion

    //region Constructors
    private AppTexts() {
    }
    //endregion

    //region Methods
    //endregion


}
