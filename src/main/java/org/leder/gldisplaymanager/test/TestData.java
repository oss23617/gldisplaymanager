package org.leder.gldisplaymanager.test;

import org.leder.gldisplaymanager.model.Display;
import org.leder.gldisplaymanager.model.Offer;
import org.leder.gldisplaymanager.model.Technology;
import org.leder.gldisplaymanager.model.Webshop;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class TestData {
    //region Constants
    public static final String OFFER_LIST_ENTRY = "%-3s € %-8s %-3s %3s Hz %5s '' %4s %4s %20s %-20s %-10s %-10s";

    //endregion

    //region Attributs
    //endregion

    //region Constructors
    //endregion

    //region Methods

    public static List<Offer> getTestOffers(){
        List<Offer> testOffers = new ArrayList<>();

        testOffers.add(new Offer(1, 100.0, new Display(1, 144, 34, Technology.OLED, 2023, "MPG 321URXDE", "MSI"), LocalDate.parse("2024-01-01"), new Webshop(1, "Cyberport", "https://www.cyberport.de" )));

        testOffers.add(new Offer(2, 200.0, new Display(2, 160, 34, Technology.OLED, 2023, "MPG 321URXDE", "MSI"), LocalDate.parse("2024-01-01"), new Webshop(2, "Cyberport", "https://www.cyberport.de" )));

        testOffers.add(new Offer(3, 300.0, new Display(3, 240, 34, Technology.OLED, 2024, "MPG 321URXDE", "MSI"), LocalDate.parse("2024-01-01"), new Webshop(3, "Cyberport", "https://www.cyberport.de" )));

        testOffers.add(new Offer(4, 400.0, new Display(4, 240, 34, Technology.OLED, 2024, "MPG 321URXDE", "MSI"), LocalDate.parse("2024-01-01"), new Webshop(4, "Cyberport", "https://www.cyberport.de" )));

        testOffers.add(new Offer(5, 400.0, new Display(5, 144, 32, Technology.IPS, 2024, "32GQ950P", "LG"), LocalDate.parse("2023-01-01"), new Webshop(5, "Cyberport", "https://www.cyberport.de" )));
        testOffers.add(new Offer(6, 400.0, new Display(6, 144, 31.5, Technology.IPS, 2022, "Nitro XV2 XV322QKKV", "Acer"), LocalDate.parse("2024-01-01"), new Webshop(6, "Cyberport", "https://www.cyberport.de" )));

        return testOffers;
    }

    //endregion


}
