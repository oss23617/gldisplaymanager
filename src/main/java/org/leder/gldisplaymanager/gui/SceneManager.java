package org.leder.gldisplaymanager.gui;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.leder.gldisplaymanager.Main;
import org.leder.gldisplaymanager.model.Offer;
import org.leder.gldisplaymanager.model.Webshop;

public class SceneManager {
    //region Constants
    //endregion

    //region Attributs
    private static SceneManager instance;
    private Stage mainStage;
    //endregion

    //region Constructors
    private SceneManager() {
    }
    //endregion

    //region Methods
    public static synchronized SceneManager getInstance() {
        if (instance == null) instance = new SceneManager();
        return instance;
    }
    public void setAndConfigureMainStage(Stage stage) {
        mainStage = stage;
        stage.setTitle("Display Price Manager!");
        openScene("fxml/overview-view.fxml");
        stage.show();
    }

    public void openScene(String fileName) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource(fileName));
            Scene scene = new Scene(fxmlLoader.load());
            mainStage.setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void openDetailScene(Offer selectedOffer) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("fxml/detailview-view.fxml"));
            Scene scene = new Scene(fxmlLoader.load());

            DetailviewController detailController = fxmlLoader.getController();
            detailController.setSelectedOfferAndDetails(selectedOffer);

            mainStage.setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void openDisplayScene() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("fxml/display-view.fxml"));
            Scene scene = new Scene(fxmlLoader.load());

            DisplayviewController displayController = fxmlLoader.getController();

            displayController.btnDelete.setDisable(true);

            mainStage.setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void openWebshopScene() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("fxml/webshop-view.fxml"));
            Scene scene = new Scene(fxmlLoader.load());

            WebshopviewController webshopController = fxmlLoader.getController();

            webshopController.btnDelete.setDisable(true);

            mainStage.setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //endregion


}
