package org.leder.gldisplaymanager.gui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import org.leder.gldisplaymanager.gui.listview.OfferCellFactory;
import org.leder.gldisplaymanager.logic.OfferManager;
import org.leder.gldisplaymanager.model.Offer;
import org.leder.gldisplaymanager.settings.AppTexts;
import org.leder.gldisplaymanager.test.TestData;

import static org.leder.gldisplaymanager.settings.AppTexts.MANUFACTURER_MODEL_YEAR;

/**
 * Controller class for FXML overview
 */
public class OverviewController {

    @FXML
    private ListView<Offer> offerListview;

    @FXML
    private TextField searchField;

    @FXML
    private void switchToDetailScene(){
        SceneManager.getInstance().openDetailScene(null);
    };

    @FXML
    private void switchToDisplayScene(){

        SceneManager.getInstance().openDisplayScene();
    };


    @FXML
    private void switchToWebshopScene(){
        SceneManager.getInstance().openWebshopScene();
    };



    public void initialize() {
        /*
        Normale Liste in eine überwachbare Liste umwandeln,
        sodass die ListView bei Änderungen benachrichtigt wird und neu lädt
         */
//        ObservableList<Offer> offerObservableList = FXCollections.observableList(TestData.getTestOffers());

        //offerListview.setItems(offerObservableList);
        filterOffers();

//        offerListview.setItems(OfferManager.getInstance().getOffers());

        //Eigene Zellenfabrik zuweisen, damit die selbst definierten Zellen aufgebaut werden
        offerListview.setCellFactory(new OfferCellFactory());

        //Den Inhalt der ListView anhand der überwachbaren Liste setzen
        offerListview.setItems(OfferManager.getInstance().getOffers());

        //Eventhandling für die ListView definieren
        offerListview.setOnMouseClicked(mouseEvent -> {
            if (mouseEvent.getButton() == MouseButton.PRIMARY && mouseEvent.getClickCount() == 2) {
                switchToDetailSceneWithSelectedOffer();
            }
        });

        offerListview.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    switchToDetailSceneWithSelectedOffer();
                }
            }
        });

        searchField.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                filterOffers();
            }
        });

    }

    private void switchToDetailSceneWithSelectedOffer() {
        Offer selectedOffer = offerListview.getSelectionModel().getSelectedItem();
        SceneManager.getInstance().openDetailScene(selectedOffer);
    }

    private void filterOffers() {
        if (searchField.getText().isBlank()) {
            offerListview.setItems(OfferManager.getInstance().getOffers());
        } else
            offerListview.setItems(OfferManager.getInstance().filterBy(searchField.getText()));
    }

    @FXML
    private void clearInput() {
        searchField.setText("");

        filterOffers();

    }

    @FXML
    private void sort(ActionEvent actionEvent) {
        if (actionEvent.getSource() instanceof Button button) {
            switch (button.getText()) {
                case AppTexts.MANUFACTURER -> OfferManager.getInstance().sortByManufacturer();
                case AppTexts.MODEL_NAME -> OfferManager.getInstance().sortByName();
                case AppTexts.MODEL_YEAR -> OfferManager.getInstance().sortByModelYear();
                case AppTexts.DATE -> OfferManager.getInstance().sortByDate();
                case MANUFACTURER_MODEL_YEAR -> OfferManager.getInstance().sortByManufacturerModelYear();
                default -> System.err.println(AppTexts.UNKNOWN_SORT_BUTTON);
            }
        }

    }

}