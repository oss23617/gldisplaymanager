package org.leder.gldisplaymanager.gui;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import org.leder.gldisplaymanager.logic.OfferManager;
import org.leder.gldisplaymanager.logic.WebshopManager;
import org.leder.gldisplaymanager.logic.database.DbManager;
import org.leder.gldisplaymanager.model.Technology;
import org.leder.gldisplaymanager.model.Webshop;

import java.util.Optional;

/**
 * Controller class for FXML webshopview
 */
public class WebshopviewController {


    @FXML
    private TextField shopNameInput;

    @FXML
    private TextField shopURLInput;

    @FXML
    private ComboBox<Webshop> webshopComboBox;


    @FXML
    Button btnDelete;

    private Webshop selectedWebshop;

    public void setSelectedWebshopAndDetails(Webshop selectedWebshop) {
        this.selectedWebshop = selectedWebshop;

//        if (this.selectedWebshop ==null){
//            btnDelete.setDisable(true);
//            return;
//        }

        btnDelete.setDisable(false);

        shopNameInput.setText(selectedWebshop.getShopname());

        shopURLInput.setText(selectedWebshop.getUrl());




    }

    public void initialize(){

        webshopComboBox.getItems().setAll(WebshopManager.getInstance().getWebshops());


        shopNameInput.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    save();
                }
            }
        });

        shopURLInput.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    save();
                }
            }
        });


        webshopComboBox.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                setSelectedWebshopAndDetails(webshopComboBox.getSelectionModel().getSelectedItem());
            }
        });

        webshopComboBox.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                setSelectedWebshopAndDetails(webshopComboBox.getSelectionModel().getSelectedItem());
            }
        });
    }

    @FXML
    private void save() {
        ObservableList<Webshop> webshops = WebshopManager.getInstance().getWebshops();

        Alert alertEmpty = new Alert(Alert.AlertType.WARNING);
        alertEmpty.setContentText("Empty fields are not valid!");

        Alert alertShopName = new Alert(Alert.AlertType.WARNING);
        alertShopName.setContentText("No valid Shop Name!");

        Alert alertShopURL = new Alert(Alert.AlertType.WARNING);
        alertShopURL.setContentText("No valid Shop URL!");

        String shopname = shopNameInput.getText();
        String shopURL = shopURLInput.getText();


        //Hier könnte ihre eventuelle Validierung stehen

        if (selectedWebshop == null) {
            //Neues Tier anlegen

            if (shopname.equals("") || shopURL.equals("") ) {

                alertEmpty.showAndWait();
                return;
            }

            Webshop newWebshop = new Webshop(shopname, shopURL);
            webshops.add(newWebshop);
        } else {
            //Ausgewähltes Tier bearbeiten
            //Referenz

//            if (checkIfValid(selectedOffer.getDisplay().getManufacturer(), manufacturer) || checkIfValid(selectedOffer.getDisplay().getModelName(), modelName) || checkIfValid(selectedOffer.getDisplay().getModelYear(), modelYear)
//            || checkIfValid(selectedOffer.getDisplay().getTechnology(), technology) || checkIfValid(selectedOffer.getDisplay().getScreenSize(), screensizeD)
//                    || checkIfValid(selectedOffer.getDisplay().getRefreshRate(), refreshRateI))
//                selectedOffer.setDisplay(new Display(refreshRateI, screensizeD, technology, modelYear, modelName, manufacturer));

            if (checkIfValid(selectedWebshop.getShopname(), shopname))
                selectedWebshop.setShopname(shopname);

            if (checkIfValid(selectedWebshop.getUrl(), shopURL))
                selectedWebshop.setUrl(shopURL);


//            if (checkIfValid(selectedOffer.getWebshop().getShopname(), shopName) || checkIfValid(selectedOffer.getWebshop().getUrl(), shopURL))
//                selectedOffer.setWebshop(new Webshop(shopName, shopURL));


            //Über Ersetzen
//            animals.set(
//                    animals.indexOf(selectedAnimal),
//                    selectedAnimal
//            );
        }

        switchToMainScene();
    }

    public void openOverView (ActionEvent actionEvent){
        SceneManager.getInstance().openScene("fxml/overview-view.fxml");
    }

    private boolean checkIfValid(String oldValue, String newValue) {
        return !oldValue.equals(newValue);
    }

    private boolean checkIfValid(int oldValue, int newValue) {
        return oldValue != newValue;
    }

    private boolean checkIfValid(double oldValue, double newValue) {
        return oldValue != newValue;
    }

    private boolean checkIfValid(Technology oldValue, Technology newValue) {
        return oldValue != newValue;
    }


    @FXML
    private void deleteWebshop() {
        Alert deleteConfirmationAlert = new Alert(Alert.AlertType.CONFIRMATION);
        deleteConfirmationAlert.setTitle("Löschen");
        deleteConfirmationAlert.setHeaderText("Do you want to delete the following Webshop?");
        deleteConfirmationAlert.setContentText(selectedWebshop.toString());
        Optional<ButtonType> optional = deleteConfirmationAlert.showAndWait();

        if (optional.isPresent() && optional.get() == ButtonType.OK) {
            WebshopManager.getInstance().getWebshops().remove(selectedWebshop);
            switchToMainScene();
        }
    }

    @FXML
    private void switchToMainScene() {
        SceneManager.getInstance().openScene("fxml/overview-view.fxml");
    }

}