package org.leder.gldisplaymanager.gui;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.Optional;

public class AlertManager {
    //region Konstanten
    //endregion

    //region Attribute
    private static AlertManager instance;
    //endregion

    //region Konstruktoren
    private AlertManager() {
    }
    //endregion

    //region Methoden
    public static synchronized AlertManager getInstance() {
        if (instance == null) instance = new AlertManager();
        return instance;
    }

    public Optional<ButtonType> showConfirmationAlert(String headerText, String contentText) {
        Alert confirmationAlert = new Alert(Alert.AlertType.CONFIRMATION);
        confirmationAlert.setHeaderText(headerText);
        confirmationAlert.setContentText(contentText);

        return confirmationAlert.showAndWait();
    }

    public void showErrorAlert(String headerText) {
        Alert errorAlert = new Alert(Alert.AlertType.ERROR);
        errorAlert.setHeaderText(headerText);
        errorAlert.show();
    }
    //endregion
}