package org.leder.gldisplaymanager.gui;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import org.leder.gldisplaymanager.logic.DisplayManager;
import org.leder.gldisplaymanager.logic.OfferManager;
import org.leder.gldisplaymanager.logic.database.DbManager;
import org.leder.gldisplaymanager.model.Display;
import org.leder.gldisplaymanager.model.Offer;
import org.leder.gldisplaymanager.model.Technology;
import org.leder.gldisplaymanager.model.Webshop;

import java.time.LocalDate;
import java.util.Optional;

/**
 * Controller class for FXML displayview
 */
public class DisplayviewController {


    @FXML
    private ComboBox<Display> displayComboBox;

    @FXML
    private TextField manufacturerInput;

    @FXML
    private TextField modelNameInput;

    @FXML
    private TextField modelYearInput;

    @FXML
    private TextField refreshRateInput;

    @FXML
    private TextField screenSizeInput;

    @FXML
    private ChoiceBox<Technology> technologyInput;



    @FXML
    Button btnDelete;

    private Display selectedDisplay;

    public void setSelectedDisplayAndDetails(Display selectedDisplay) {
        this.selectedDisplay = selectedDisplay;

//        if (this.selectedDisplay==null){
//            btnDelete.setDisable(true);
//            return;
//        }

        btnDelete.setDisable(false);

        manufacturerInput.setText(selectedDisplay.getManufacturer());

        modelNameInput.setText(selectedDisplay.getModelName());
        modelYearInput.setText(String.valueOf(selectedDisplay.getModelYear()));
        refreshRateInput.setText(String.valueOf(selectedDisplay.getRefreshRate()));
        screenSizeInput.setText(String.valueOf(selectedDisplay.getScreenSize()));

        technologyInput.setValue(selectedDisplay.getTechnology());




    }

    public void initialize(){

        displayComboBox.getItems().setAll(DisplayManager.getInstance().getDisplays());

        technologyInput.getItems().setAll(Technology.values());

        manufacturerInput.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    save();
                }
            }
        });

        modelNameInput.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    save();
                }
            }
        });


        modelYearInput.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    save();
                }
            }
        });


        refreshRateInput.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    save();
                }
            }
        });


        screenSizeInput.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    save();
                }
            }
        });



        technologyInput.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    save();
                }
            }
        });


        displayComboBox.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                setSelectedDisplayAndDetails(displayComboBox.getSelectionModel().getSelectedItem());
            }
        });

        displayComboBox.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                setSelectedDisplayAndDetails(displayComboBox.getSelectionModel().getSelectedItem());
            }
        });
    }

    @FXML
    private void save() {
        ObservableList<Display> displays = DisplayManager.getInstance().getDisplays();

        Alert alertEmpty = new Alert(Alert.AlertType.WARNING);
        alertEmpty.setContentText("Empty fields are not valid!");

        Alert alertModelYear = new Alert(Alert.AlertType.WARNING);
        alertModelYear.setContentText("No valid Modelyear!");

        Alert alertScreensize = new Alert(Alert.AlertType.WARNING);
        alertScreensize.setContentText("No valid Screensize!");

        Alert alertRefreshRate = new Alert(Alert.AlertType.WARNING);
        alertRefreshRate.setContentText("No valid Refreshrate!");


        int modelYear;



        String manufacturer = manufacturerInput.getText();
        String modelName = modelNameInput.getText();

        String refreshRate = refreshRateInput.getText();

        int refreshRateI;

        String screensize = screenSizeInput.getText();

        double screensizeD;

        Technology technology = technologyInput.getSelectionModel().getSelectedItem();


        try {
            modelYear = Integer.parseInt(modelYearInput.getText());

        }catch (NumberFormatException ex) {
            ex.printStackTrace();
            alertModelYear.showAndWait();
            return;
        }


        try {
            screensizeD = Double.parseDouble(screensize);

        }catch (NumberFormatException ex) {
            ex.printStackTrace();
            alertScreensize.showAndWait();
            return;
        }




        try {
            refreshRateI = Integer.parseInt(refreshRate);

        }catch (NumberFormatException ex) {
            ex.printStackTrace();
            alertRefreshRate.showAndWait();
            return;
        }


        //Hier könnte ihre eventuelle Validierung stehen

        if (selectedDisplay == null) {
            //Neues Tier anlegen

            if (manufacturer.equals("") || modelName.equals("") || technology == null) {

                alertEmpty.showAndWait();
                return;
            }

            Display newDisplay = new Display(refreshRateI, screensizeD, technology, modelYear, modelName, manufacturer);
            displays.add(newDisplay);
        } else {
            //Ausgewähltes Tier bearbeiten
            //Referenz

//            if (checkIfValid(selectedOffer.getDisplay().getManufacturer(), manufacturer) || checkIfValid(selectedOffer.getDisplay().getModelName(), modelName) || checkIfValid(selectedOffer.getDisplay().getModelYear(), modelYear)
//            || checkIfValid(selectedOffer.getDisplay().getTechnology(), technology) || checkIfValid(selectedOffer.getDisplay().getScreenSize(), screensizeD)
//                    || checkIfValid(selectedOffer.getDisplay().getRefreshRate(), refreshRateI))
//                selectedOffer.setDisplay(new Display(refreshRateI, screensizeD, technology, modelYear, modelName, manufacturer));

            if (checkIfValid(selectedDisplay.getManufacturer(), manufacturer))
                selectedDisplay.setManufacturer(manufacturer);

            if (checkIfValid(selectedDisplay.getModelName(), modelName))
                selectedDisplay.setModelName(modelName);


            if (checkIfValid(selectedDisplay.getModelYear(), modelYear))
                selectedDisplay.setModelYear(modelYear);

            if (checkIfValid(selectedDisplay.getTechnology(), technology))
                selectedDisplay.setTechnology(technology);


            if (checkIfValid(selectedDisplay.getScreenSize(), screensizeD))
                selectedDisplay.setScreenSize(screensizeD);


            if (checkIfValid(selectedDisplay.getRefreshRate(), refreshRateI))
                selectedDisplay.setRefreshRate(refreshRateI);


//            if (checkIfValid(selectedOffer.getWebshop().getShopname(), shopName) || checkIfValid(selectedOffer.getWebshop().getUrl(), shopURL))
//                selectedOffer.setWebshop(new Webshop(shopName, shopURL));


            //Über Ersetzen
//            animals.set(
//                    animals.indexOf(selectedAnimal),
//                    selectedAnimal
//            );
        }

        switchToMainScene();
    }

    public void openOverView (ActionEvent actionEvent){
        SceneManager.getInstance().openScene("fxml/overview-view.fxml");
    }

    private boolean checkIfValid(String oldValue, String newValue) {
        return !oldValue.equals(newValue);
    }

    private boolean checkIfValid(int oldValue, int newValue) {
        return oldValue != newValue;
    }

    private boolean checkIfValid(double oldValue, double newValue) {
        return oldValue != newValue;
    }

    private boolean checkIfValid(Technology oldValue, Technology newValue) {
        return oldValue != newValue;
    }

    @FXML
    private void deleteDisplay() {
        Alert deleteConfirmationAlert = new Alert(Alert.AlertType.CONFIRMATION);
        deleteConfirmationAlert.setTitle("Löschen");
        deleteConfirmationAlert.setHeaderText("Do you want to delete the following Display?");
        deleteConfirmationAlert.setContentText(selectedDisplay.toString());
        Optional<ButtonType> optional = deleteConfirmationAlert.showAndWait();

        if (optional.isPresent() && optional.get() == ButtonType.OK) {
            DisplayManager.getInstance().getDisplays().remove(selectedDisplay);
            switchToMainScene();
        }
    }

    @FXML
    private void switchToMainScene() {
        SceneManager.getInstance().openScene("fxml/overview-view.fxml");
    }

}