package org.leder.gldisplaymanager.gui;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import org.leder.gldisplaymanager.logic.DisplayManager;
import org.leder.gldisplaymanager.logic.OfferManager;
import org.leder.gldisplaymanager.logic.WebshopManager;
import org.leder.gldisplaymanager.logic.database.DbManager;
import org.leder.gldisplaymanager.model.Display;
import org.leder.gldisplaymanager.model.Offer;
import org.leder.gldisplaymanager.model.Technology;
import org.leder.gldisplaymanager.model.Webshop;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.Optional;

/**
 * Controller class for FXML detailview
 */
public class DetailviewController {

    @FXML
    private DatePicker datepickerInput;

    @FXML
    private TextField priceInput;

    @FXML
    private ComboBox<Display> displayComboBox;

    @FXML
    private ComboBox<Webshop> webshopComboBox;

    @FXML
    private Button btnDelete;

    private Offer selectedOffer;
    public void setSelectedOfferAndDetails(Offer selectedOffer) {
        this.selectedOffer = selectedOffer;

        if (this.selectedOffer==null){
            btnDelete.setDisable(true);
            return;
        }

        datepickerInput.setValue(selectedOffer.getDate());

        priceInput.setText(String.valueOf(selectedOffer.getPrice()));

        displayComboBox.setValue(selectedOffer.getDisplay());

        webshopComboBox.setValue(selectedOffer.getWebshop());


    }

    public void initialize(){

//        technologyInput.getItems().setAll(Technology.values());
        displayComboBox.getItems().setAll(DisplayManager.getInstance().getDisplays());

        webshopComboBox.getItems().setAll(WebshopManager.getInstance().getWebshops());

        datepickerInput.setValue(LocalDate.now());

        datepickerInput.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    save();
                }
            }
        });

        priceInput.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    save();
                }
            }
        });


    }

    @FXML
    private void save() {
        ObservableList<Offer> offers = OfferManager.getInstance().getOffers();

        Alert alertEmpty = new Alert(Alert.AlertType.WARNING);
        alertEmpty.setContentText("Empty fields are not valid!");

        Alert alertPrice = new Alert(Alert.AlertType.WARNING);
        alertPrice.setContentText("No valid Price!");


        int modelYear;

        LocalDate date = datepickerInput.getValue();

        double priceD;

        int displayID;

        int webshopID;

        Display display;

        Webshop webshop;


        displayID = displayComboBox.getSelectionModel().getSelectedItem().getId();

        webshopID = webshopComboBox.getSelectionModel().getSelectedItem().getId();

        display = displayComboBox.getSelectionModel().getSelectedItem();

        webshop = webshopComboBox.getSelectionModel().getSelectedItem();


        try {
            priceD = Double.parseDouble(priceInput.getText());

        }catch (NumberFormatException ex) {
            ex.printStackTrace();
            alertPrice.showAndWait();
            return;
        }


        //Hier könnte ihre eventuelle Validierung stehen

        if (selectedOffer == null) {
            //Neues Tier anlegen

            if (datepickerInput.getValue() == null){

                alertEmpty.showAndWait();
                return;
            }

            Offer newOffer = new Offer(priceD, display, date, webshop);
            offers.add(newOffer);
        } else {
            //Ausgewähltes Tier bearbeiten
            //Referenz
            if (checkIfValid(selectedOffer.getPrice(), priceD))
                selectedOffer.setPrice(priceD);

            if (checkIfValid(selectedOffer.getDate(), date))
                selectedOffer.setDate(date);

            if (checkIfValid(selectedOffer.getDisplay().getId(), displayID)){
                selectedOffer.setDisplay(display);
            }


            if (checkIfValid(selectedOffer.getWebshop().getId(), webshopID)){
                selectedOffer.setWebshop(webshop);
            }


            DbManager.getInstance().update(selectedOffer);

//            if (checkIfValid(selectedOffer.getDisplay().getManufacturer(), manufacturer) || checkIfValid(selectedOffer.getDisplay().getModelName(), modelName) || checkIfValid(selectedOffer.getDisplay().getModelYear(), modelYear)
//            || checkIfValid(selectedOffer.getDisplay().getTechnology(), technology) || checkIfValid(selectedOffer.getDisplay().getScreenSize(), screensizeD)
//                    || checkIfValid(selectedOffer.getDisplay().getRefreshRate(), refreshRateI))
//                selectedOffer.setDisplay(new Display(refreshRateI, screensizeD, technology, modelYear, modelName, manufacturer));


//            if (checkIfValid(selectedOffer.getWebshop().getShopname(), shopName) || checkIfValid(selectedOffer.getWebshop().getUrl(), shopURL))
//                selectedOffer.setWebshop(new Webshop(shopName, shopURL));



            //Über Ersetzen
//            animals.set(
//                    animals.indexOf(selectedAnimal),
//                    selectedAnimal
//            );
        }

        switchToMainScene();
    }

    public void openOverView (ActionEvent actionEvent){
        SceneManager.getInstance().openScene("fxml/overview-view.fxml");
    }

    private boolean checkIfValid(String oldValue, String newValue) {
        return !oldValue.equals(newValue);
    }

    private boolean checkIfValid(int oldValue, int newValue) {
        return oldValue != newValue;
    }

    private boolean checkIfValid(double oldValue, double newValue) {
        return oldValue != newValue;
    }

    private boolean checkIfValid(LocalDate oldValue, LocalDate newValue) {
        return oldValue != newValue;
    }


    private boolean checkIfValid(Technology oldValue, Technology newValue) {
        return oldValue != newValue;
    }
    @FXML
    private void deleteOffer() {
        Alert deleteConfirmationAlert = new Alert(Alert.AlertType.CONFIRMATION);
        deleteConfirmationAlert.setTitle("Löschen");
        deleteConfirmationAlert.setHeaderText("Do you want to delete the following Offer?");
        deleteConfirmationAlert.setContentText(selectedOffer.toString());
        Optional<ButtonType> optional = deleteConfirmationAlert.showAndWait();

        if (optional.isPresent() && optional.get() == ButtonType.OK) {
            OfferManager.getInstance().getOffers().remove(selectedOffer);
            switchToMainScene();
        }
    }

    @FXML
    private void switchToMainScene() {
        SceneManager.getInstance().openScene("fxml/overview-view.fxml");
    }

}