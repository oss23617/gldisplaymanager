package org.leder.gldisplaymanager.gui.listview;

import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;
import org.leder.gldisplaymanager.model.Offer;

import java.util.List;


public class OfferCellFactory implements Callback<ListView<Offer>, ListCell<Offer>> {
    //region Constants
    //endregion

    //region Attributs
    //endregion

    //region Constructors
    //endregion

    //region Methods

    @Override
    public ListCell<Offer> call(ListView<Offer> param){
        return new OfferCell<Offer>();
    }
    //endregion


}
