package org.leder.gldisplaymanager.gui.listview;

import javafx.geometry.Insets;
import javafx.scene.control.ListCell;
import javafx.scene.text.Font;
import org.leder.gldisplaymanager.model.Offer;
import org.leder.gldisplaymanager.test.TestData;

import java.util.Objects;

public class OfferCell<O> extends ListCell<Offer> {
    //region Konstanten
    //endregion

    //region Attribute
    //endregion

    //region Konstruktoren
    //endregion

    //region Methoden

    @Override
    protected void updateItem(Offer offer, boolean isEmpty) {
        super.updateItem(offer, isEmpty);

        if (offer == null || isEmpty) {
            setText(null);
            setGraphic(null);
            return;
        }

        setText(String.format(TestData.OFFER_LIST_ENTRY,
                offer.getId(), offer.getPrice(), offer.getDisplay().getId(), offer.getDisplay().getRefreshRate(), offer.getDisplay().getScreenSize(), offer.getDisplay().getTechnology(), offer.getDisplay().getModelYear(), offer.getDisplay().getModelName(), offer.getDisplay().getManufacturer(), offer.getDate(), offer.getWebshop().getShopname()));


        //load monospaced Linux font:
        setFont(Font.loadFont(Objects.requireNonNull(getClass().getResource("/fonts/UbuntuMono-Regular.ttf")).toExternalForm(), 18));



        setPadding(new Insets(10, 5, 10, 5));
    }

}