module org.leder.gldisplaymanager {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;


    opens org.leder.gldisplaymanager to javafx.fxml;
    exports org.leder.gldisplaymanager;
    exports org.leder.gldisplaymanager.gui;
    opens org.leder.gldisplaymanager.gui to javafx.fxml;
}