## docker howto phpmyadmin + mariadb for arch Linux w/ ZSH

`pamac update`

`pamac install fuse-overlayfs`
`pamac install netavark`
`pamac install docker-buildx`

`pamac install docker`
`pamac install docker-compose`

`pamac install dialog`

`sudo groupadd docker`

`sudo usermod -aG docker $USER`

`newgrp docker`

`docker run hello-world`

`docker run -it --rm archlinux bash -c "echo hello world" `

`mkdir ~/Dokumente; mkdir ~/Dokumente/Projekt; mkdir ~/Dokumente/Projekt/Docker`

`gedit ~/Dokumente/Projekt/Docker/docker-compose.yml`

`mkdir ~/bin`

`gedit ~/.zshenv`

`gedit bin/docker_start`
`gedit bin/docker_up`
`gedit bin/docker_ls`

`./bin/docker_start`

`./bin/docker_up`

`./bin/docker_ls`

`https://localhost:8000`

login: root - pw: qwertz

import SQL: `display_manager (1).sql`

`mkdir ~/IdeaProjects/`

`cd ~/IdeaProjects/`

`git clone git@gitlab.com:oss23617/gldisplaymanager.git`

`gedit ~/bin/gldpm`

`~/bin/gldpm &`

*** 
~/Dokumente/Projekt/Docker/docker-compose.yml

```
version: "3.7"

services:

  web:
    image: nginx:1.17
    ports:
      - 80:80
    volumes:
      - /var/www/docker-study.loc/recipe-03/php:/var/www/myapp
      - /var/www/docker-study.loc/recipe-03/docker/site.conf:/etc/nginx/conf.d/site.conf
    depends_on:
      - php
      - mariadb

  php:
    image: php:7.2-fpm
    volumes:
      - /var/www/docker-study.loc/recipe-03/php:/var/www/myapp
      - /var/www/docker-study.loc/recipe-03/docker/php.ini:/usr/local/etc/php/php.ini
    depends_on:
      - mariadb

  mariadb:
    image: mariadb:10.4
    ports: 
      - 3306:3306
    restart: always
    volumes:
      - mariadb-data:/var/lib/mysql
    environment:
      MYSQL_ROOT_PASSWORD: qwertz

  phpmyadmin:
    image: phpmyadmin/phpmyadmin:latest
    ports:
      - 8000:80
    environment:
      - PMA_ARBITRARY=1
      - PMA_HOST=mariadb
    depends_on:
      - mariadb

volumes:
  mariadb-data:
```

*** 
~/bin/docker_start

```
#!/bin/zsh

sudo systemctl start docker

```

*** 
~/bin/docker_up

```
#!/bin/zsh

cd ~/Dokumente/Projekt/Docker/
docker compose up -d 
```

*** 
~/bin/docker_ls

```
#!/bin/zsh

docker container ls  
```

*** 
~/bin/gldpm

```
#!/bin/zsh

cd ~/IdeaProjects/GlDisplayManager/

mvn clean javafx:run

```

***
`chmod +x ~/bin/*`  
***
~/.zshenv

```
path+=('/home/$USER/bin')
export PATH
```
restart cmdline `CTRL + d`